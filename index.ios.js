/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Linking
} from 'react-native';

class LinkingTest extends Component {
    
    click(){
        
        let url = 'www.spiegel.de'
        console.log('URL', url)
        
        Linking.canOpenURL(url).then(supported => {
        if (!supported) {
            console.log('Can\'t handle url: ' + url);
        } else {
            console.log('Callback')
            return Linking.openURL(url);
        }
        }).catch(err => console.error('An error occurred', err));
        
        Linking.openURL(url).then((x) =>{
            console.log('OK',x )
        } )
        
        Linking.openURL(url).catch(err => console.error('An error occurred', err));
        

        
    }
    
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native!
        </Text>
        <TouchableOpacity onPress={this.click.bind(this)}>
        <Text style={styles.instructions}>
          To get started, edit index.ios.js
        </Text>
        </TouchableOpacity>
        <Text style={styles.instructions}>
          Press Cmd+R to reload,{'\n'}
          Cmd+D or shake for dev menu
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('LinkingTest', () => LinkingTest);
